%*********************************Generalized Moving Peaks Benchmark (GMPB)_SIMPLIFIED VERSION******
%Author: Danial Yazdani
%Last Edited: June 03, 2021
%
% ------------
% Reference:
% ------------
%
%  D. Yazdani et al.,
%            "Benchmarking Continuous Dynamic Optimization: Survey and Generalized Test Suite,"
%            IEEE Transactions on Cybernetics (2020).
% 
% --------
% License:
% --------
% This program is to be used under the terms of the GNU General Public License 
% (http://www.gnu.org/copyleft/gpl.html).
% Author: Danial Yazdani
% e-mail: danial.yazdani AT gmail dot com
%         danial.yazdani AT yahoo dot com
% Copyright notice: (c) 2021 Danial Yazdani
%**************************************************************************************************
function [result] = DUMMYfitness(X,Problem)
[SolutionNumber,~] = size(X);
result = NaN(SolutionNumber,1);
for jj=1 : SolutionNumber
    x = X(jj,:)';
    f=NaN(1,Problem.PeakNumber);
    for k=1 : Problem.PeakNumber
        a = Transform((x - Problem.PeaksPosition(k,:,Problem.Environmentcounter)')'*Problem.RotationMatrix{Problem.Environmentcounter}(:,:,k)',Problem.tau(Problem.Environmentcounter,k),Problem.eta(k,:,Problem.Environmentcounter));
        b = Transform(Problem.RotationMatrix{Problem.Environmentcounter}(:,:,k) * (x - Problem.PeaksPosition(k,:,Problem.Environmentcounter)'),Problem.tau(Problem.Environmentcounter,k),Problem.eta(k,:,Problem.Environmentcounter));
        f(k) = Problem.PeaksHeight(Problem.Environmentcounter,k) - sqrt( a * diag(Problem.PeaksWidth(k,:,Problem.Environmentcounter).^2) * b);
    end
    result(jj) = max(f);
end
end

function Y = Transform(X,tau,eta)
Y = X;
tmp = (X > 0);
Y(tmp) = log(X(tmp));
Y(tmp) = exp(Y(tmp) + tau*(sin(eta(1).*Y(tmp)) + sin(eta(2).*Y(tmp))));
tmp = (X < 0);
Y(tmp) = log(-X(tmp));
Y(tmp) = -exp(Y(tmp) + tau*(sin(eta(3).*Y(tmp)) + sin(eta(4).*Y(tmp))));
end